<?php
	
	if(strpos(shell_exec('/usr/local/apache/bin/apachectl -l'), 'mod_rewrite') !== false)
	{
		die('Mod rewrite has to be available for this application.');
	}
	
	# Include permission to include other files
	$inc = true; 
	
	try
	{
		# Load configuration and general functions
		# Define required files
		$required_files = array('config.php', 'functions.php');
		for ($i = 0; $i < count($required_files); $i++)
		{
			if (file_exists($required_files[$i]))
			{
				require($required_files[$i]);
			}
			else
			{
				throw new Exception($required_files[$i].' not found.');
			}
		}
		
		# Sanitizing all global variables
		sanitize($_GET, array(
				'section' => 'str',
				'restart' => 'int'
		));
		sanitize($_SERVER, array(
				'REQUEST_URI' => 'str'
		));
		unset($_POST);
		unset($_SESSION);
		unset($_FILES);
		
		# Get Section
		$request = $_SERVER['REQUEST_URI'];
		if (isset($request))
		{
			$section = trim($request, '/');
			$section = substr($section, strlen(FOLDER));
			$url_split = explode('#', $section);
			$section = $url_split[0];
			$url_split = explode('?', $section);
			$section = $url_split[0];
			$url_split = explode('/', $section);
			$section = $url_split[0]; # section
			$section_parts = $url_split; # sub sections
			unset($url_split);
			if(empty($section_parts[0])) unset($section_parts[0]);
		}
		
		# Load all folders into an array
		switch(count($section_parts)) 
		{
			case 0: # home
				if($folder_list = get_folders(dirname(__FILE__).'/res/'))
				{
					$folder_list = array_flip($folder_list);
					foreach($folder_list as $folder => &$value)
					{
						$value = get_folders(dirname(__FILE__).'/res/'.$folder.'/');
						$value = array_flip($value);
						foreach($value as $subfolder => &$value2)
						{
							$subfolder_path = 'res/'.$folder.'/'.$subfolder;
							$data = get_data($subfolder_path.'/data.txt');
							if($data !== false)
							{
								if(empty($data['page'])) $data['page'] = 0;
								$data['maxpage'] = maxpage(dirname(__FILE__).'/'.$subfolder_path);
								$value2 = $data;
							} else {
								$i = array_search($subfolder, $value);
								unset($value[$i]);						
							}
						}
					}
				}
				include 'tpl.php';
				break;
			case 1: # Year
				if($folder_list = get_folders(dirname(__FILE__).'/res/'.$section_parts[0].'/'))
				{
					$folder_list = array_flip($folder_list);
					foreach($folder_list as $folder => &$value)
					{
						$subfolder_path = 'res/'.$section_parts[0].'/'.$folder;
						$data = get_data($subfolder_path.'/data.txt');
						if($data !== false)
						{
							if(empty($data['page'])) $data['page'] = 0;
							$data['maxpage'] = maxpage(dirname(__FILE__).'/'.$subfolder_path);
							$value = $data;
						} else {
							$i = array_search($folder, $folder_list);
							unset($folder_list[$i]);						
						}
					}
				}
				include 'tpl.php';
				break;
			case 2: # Relocate
				$path = $section_parts[0].'/'.$section_parts[1];
				$data = get_data('res/'.$path.'/data.txt');
				if(empty($data['page']) || isset($_GET['restart'])) $data['page'] = 1;
				header('Location: '.HOME.$path.'/'.$data['page']);
				break;
			case 3: # Page
				$path = $section_parts[0].'/'.$section_parts[1];
				if($filename = get_filename('res/'.$path, $section_parts[2]))
				{
					$data = get_data('res/'.$path.'/data.txt');
					if(is_numeric($section_parts[2]) && $section_parts[2] <= maxpage(dirname(__FILE__).'/res/'.$path)) set_data('res/'.$path.'/data.txt', 'page', $section_parts[2]);
					$file = file_get_contents('res/'.$path.'/'.$filename.'.html');
					$search[] = '[respath]';
					$replace[] = HOME.'res/'.$path;
					$search[] = '[navigation]';
					$replace[] = make_navigation($path, $filename);
					$search[] = '[folder]';
					$replace[] = HOME.$path;
					$file = str_replace($search, $replace, $file);
					echo $file;
				} else {
					header('Location: '.HOME);
				}
				break;
			default:
				include 'tpl.php';
				break;
		}		
	}
	catch (Exception $e)
	{
		$error = $e->getMessage(); # set the error code of the exception
		echo $error;
	}

 ?>