<?php 
	if(empty($inc)) exit;
?><!DOCTYPE html>
<html lang="de">
 <head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="<?php echo HOME; ?>style.css">
  <link rel="shortcut icon" href="<?php echo DOMAIN; ?>favicon.ico">
 </head>
 <body>
<?php
	if (count($section_parts) == 0) {
?>
  <h1>Learning</h1>
  <ul>
<?php
		foreach ($folder_list as $folder => &$value)
		{
?>
   <li>
    <?php echo $folder; ?>
<?php
			if (!empty($value))
			{
?>
	 <ul>
<?php
			foreach ($value as $subfolder => &$value2)
			{
?>
      <li>
       <a href="<?php echo HOME.$folder.'/'.$subfolder; ?>"><?php echo $value2['name']; ?></a> 
	   (<?php echo $value2['page']; ?>/<?php echo $value2['maxpage']; ?>)
	   <?php if(!empty($value2['description'])) echo ' - '.$value2['description']; ?>
	   (<a href="<?php echo HOME.$folder.'/'.$subfolder; ?>?restart">Restart</a>)
      </li>
<?php		} ?>
	 </ul>
<?php		} ?>
   </li>
<?		} ?>
 </ul>
<?php
	} elseif (count($section_parts) == 1) {
?>
  <h1><?php echo $section_parts[0]; ?></h1>
  <ul>
<?php
		foreach ($folder_list as $folder => &$value)
		{
?>
   <li>
    <a href="<?php echo HOME.$section_parts[0].'/'.$folder; ?>"><?php echo $value['name']; ?></a> 
	(<?php echo $value['page']; ?>/<?php echo $value['maxpage']; ?>)
	<?php if(!empty($value['description'])) echo ' - '.$value['description']; ?>
	(<a href="<?php echo HOME.$section_parts[0].'/'.$folder; ?>?restart">Restart</a>)
   </li>
<?		} ?>
 </ul>
<?php
	} elseif (count($section_parts) > 1) {
?>

<?php
	}
?>
 </body>
</html>