<?php

	# Sanitize a string
	function san ($var, $type)
	{
		switch ($type) 
		{
			case 'int': # integer
				$var = (int) $var;
				break;
			case 'no':
				break;
			case 'str': # trim string
				$var = trim($var);
				break;
			case 'password': # password, trim string, no HTML, no PHP
				$var = stripslashes(htmlentities(trim($var)));
				break;
			case 'nohtml': # trim string, no HTML allowed
				$var = htmlentities(trim($var), ENT_QUOTES);
				break;
			case 'plain': # trim string, no HTML allowed, plain text
				$var = strip_tags(htmlentities(trim($var), ENT_NOQUOTES));
				break;
			case 'sql':
				return mysql_real_escape_string($var);
				break;
		}
		return $var;
	}
	
	# Sanitize an array
	function sanitize(&$data, $whatToKeep)
	{
		$data = array_intersect_key($data, $whatToKeep); 
		foreach ($data as $key => $value)
		{
			$data[$key] = san($data[$key], $whatToKeep[$key]);
		}
	}
	
	function get_folders ($path)
	{
		$results = scandir($path);
		
		if(empty($results)) return false;
		foreach ($results as $result) {
			if ($result === '.' or $result === '..') continue;

			if (is_dir($path . '/' . $result)) {
				$arr[] = $result;
			}
		}
		if(empty($arr)) $arr = array();
		return $arr;
	}
	
	function maxpage($path)
	{
		$results = scandir($path);
		
		if (empty($results)) return 0;
		foreach ($results as $result) {
			if ($result === '.' or $result === '..') continue;

			if (is_file($path . '/' . $result)) {
				$exp = explode(".", $result);
				if (count($exp) > 1) $filetype = $exp[1];
				else continue;
				if($filetype != 'html') continue;
				$arr[] = $result;
			}
		}
		if(empty($arr)) return 0;
		return count($arr);
	}
	
	function get_data ($path)
	{
		if (!file_exists($path)) return false;
		$file = file_get_contents($path);
		$exp = explode("\n", $file);
		foreach ($exp as $key => $value)
		{
			$exp2 = explode(":", $value);
			if (count($exp2) > 1)
			{
				$arr[$exp2[0]] = $exp2[1];
			}
		}
		return $arr;
	}
	
	function put_data ($path, $data)
	{
		foreach($data as $key => &$value)
		{
			$value = $key.':'.$value;
		}
		$put = implode("\n", $data);
		return file_put_contents($path, $put);
	}
	
	function set_data ($path, $key, $value)
	{
		$data = get_data($path);
		$data[$key] = $value;
		put_data($path, $data);
	}
	
	function get_filename ($path, $number)
	{
		if (file_exists($path.'/'.$number.'.html')) return $number;
		if (strlen($number) > 10) return false;
		return get_filename($path, '0'.$number);
	}
	
	function make_navigation ($path, $number)
	{
		$maxpage = maxpage(dirname(__FILE__).'/res/'.$path);
		$prev = $number - 1;
		$next = $number + 1;
		$filename = get_filename('res/'.$path, $number);
		$output = '<ul id="navigation">'."\n";
		$output .= ' <li class="top"><a href="'.HOME.'">&#8657;</a></li>'."\n";
		if($number > 1) {
			$output .= ' <li class="prev"><a href="'.HOME.$path.'/'.$prev.'">&#8592;</a></li>'."\n";
		}
		if($number < $maxpage) {
			$output .= ' <li class="next"><a href="'.HOME.$path.'/'.$next.'">&#8594;</a></li>'."\n";
		}
		if($number == $maxpage) {
			$output .= ' <li class="restart"><a href="'.HOME.$path.'?restart">&#8634;</a></li>'."\n";
		}
		$output .= '</ul>'."\n";
		return $output;
	}
?>